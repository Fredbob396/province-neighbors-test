﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace adjtest
{
    class Globals
    {
        public static DirectBitmap Image;
        public static Dictionary<Tuple<int, int>, Point> Points;
        public static List<Province> Provinces;
        public static List<Definition> Definition;
    }
}
