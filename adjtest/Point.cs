﻿using System.Drawing;

namespace adjtest
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Color Color { get; set; }

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }
}
