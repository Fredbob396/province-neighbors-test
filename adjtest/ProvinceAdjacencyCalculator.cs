﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Text;

namespace adjtest
{
    /* TODO:
     * Load Areas/Regions/Superregions from files
     * Associate Provinces/Areas/Regions/Superregions with eachother
     * Calculate Neighbors for every Area/Region/Superregion and dump the results to a database
     * Hook up a process of some kind that would create a new relationship database when a map update comes out
    */
    class ProvinceAdjacencyCalculator
    {
        private static readonly Dictionary<Point, Point> ClockwiseOffset = new Dictionary<Point, Point>()
        {
             { new Point { X = 1, Y = 0 }, new Point { X = 1, Y = -1 } },     // right        => down-right
             { new Point { X = 1, Y = -1 }, new Point { X = 0, Y = -1 } },    // down-right   => down
             { new Point { X = 0, Y = -1 }, new Point { X = -1, Y = -1 } },   // down         => down-left
             { new Point { X = -1, Y = -1 }, new Point { X = -1, Y = 0 } },   // down-left    => left
             { new Point { X = -1, Y = 0 }, new Point { X = -1, Y = 1 } },    // left         => top-left
             { new Point { X = -1, Y = 1 }, new Point { X = 0, Y = 1 } },     // top-left     => top
             { new Point { X = 0, Y = 1 }, new Point { X = 1, Y = 1 } },      // top          => top-right
             { new Point { X = 1, Y = 1 }, new Point { X = 1, Y = 0 } }       // top-right    => right
        };

        public static void Run()
        {
            Console.WriteLine("LoadDefinition");
            Globals.Definition = LoadDefinition();

            Console.WriteLine("GetMapPixels");
            Globals.Points = GetMapPixels();

            Console.WriteLine("GetProvincesFromDefinition");
            Globals.Provinces = GetProvincesFromDefinition();

            Console.WriteLine("GetAllProvinceNeighbors");
            GetAllProvinceNeighbors();

            Console.WriteLine("DumpNeighborsToDatabase");
            DumpNeighborsToDatabase();
        }

        private static List<Definition> LoadDefinition()
        {
            var definition = new List<Definition>();
            var definitionFile = File.ReadAllLines("definition.csv", Encoding.Default);
            bool first = true;
            foreach (string line in definitionFile)
            {
                // Skip header
                if (first) { first = false; continue; }

                var def = line.Split(';');
                definition.Add(new Definition
                {
                    ProvinceId = int.Parse(def[0]),
                    Color = Color.FromArgb(255, int.Parse(def[1]), int.Parse(def[2]), int.Parse(def[3])),
                    ProvinceName = def[4]
                });
            }
            return definition;
        }

        // TODO: Very slow since adding Tuple. Find a way to speed it up?
        private static Dictionary<Tuple<int, int>, Point> GetMapPixels()
        {
            var pixels = new Dictionary<Tuple<int, int>, Point>();
            using (var tempBitmap = new Bitmap("provinces_mega.png"))
            {
                Globals.Image = new DirectBitmap(tempBitmap.Width, tempBitmap.Height);

                for (int y = 0; y < tempBitmap.Height; y++)
                {
                    for (int x = 0; x < tempBitmap.Width; x++)
                    {
                        var color = tempBitmap.GetPixel(x, y);
                        Globals.Image.SetPixel(x, y, color);
                        pixels.Add(Tuple.Create(x, y), new Point { X = x, Y = y, Color = color });
                    }
                }
            }

            return pixels;
        }

        private static List<Province> GetProvincesFromDefinition()
        {
            var provinces = new List<Province>();
            var colorGroupings = Globals.Points.GroupBy(x => x.Value.Color).ToList();

            foreach (Definition definition in Globals.Definition)
            {
                var defPointsEnum = colorGroupings.FirstOrDefault(x => x.Key.Equals(definition.Color));
                if (defPointsEnum == null)
                {
                    continue;
                }

                var province = new Province
                {
                    Id = definition.ProvinceId,
                    Name = definition.ProvinceName,
                    Color = definition.Color
                };

                foreach (var keyValuePair in defPointsEnum)
                {
                    province.Points.Add(keyValuePair.Key, keyValuePair.Value);
                }

                provinces.Add(province);
            }

            return provinces;
        }

        private static void GetAllProvinceNeighbors()
        {
            foreach (Province province in Globals.Provinces)
            {
                Console.WriteLine($"Province #{province.Id} Start...");
                province.Neighbors = GetProvinceNeighbors(province);
                Console.WriteLine($"Province #{province.Id} Done!");
            }
        }

        private static void DumpNeighborsToDatabase()
        {
            using (var conn = new SQLiteConnection("Data Source=Adjacencies.db"))
            {
                conn.Open();
                using (var cmd = new SQLiteCommand(conn))
                using (var transaction = conn.BeginTransaction())
                {
                    foreach (var province in Globals.Provinces)
                    {
                        // First insert regular province...
                        cmd.CommandText = "INSERT INTO Provinces VALUES(@ID, @NAME)";
                        cmd.Parameters.AddWithValue("@ID", province.Id);
                        cmd.Parameters.AddWithValue("@NAME", province.Name);
                        cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();

                        // Then insert all it's neighbors
                        cmd.CommandText = "INSERT INTO ProvinceAdjacencies VALUES(@ID, @NEIGHBOR)";
                        foreach (var neighbor in province.Neighbors)
                        {
                            cmd.Parameters.AddWithValue("@ID", province.Id);
                            cmd.Parameters.AddWithValue("@NEIGHBOR", neighbor.Id);
                            cmd.ExecuteNonQuery();

                            cmd.Parameters.Clear();
                        }
                    }
                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Gets all province neighbors using the Moore Neighboorhood tracing algorithm
        /// https://github.com/Dkendal/Moore-Neighbor_Contour_Tracer/blob/master/ContourTrace.cs
        /// https://en.wikipedia.org/wiki/Moore_neighborhood#Algorithm
        /// </summary>
        private static List<Province> GetProvinceNeighbors(Province province)
        {
            //TestConsoleBuffer();
            var outline = new List<Point>();
            Color targetColor = province.Color;
            List<List<Point>> chunks = province.GetProvinceChunks();

            foreach (List<Point> chunk in chunks)
            {
                Point beforeFirstPixel = null;
                Point firstPixel = null;
                Point boundaryPixel;
                Point currentPixel;
                Point backtrackPixel;
                // Find First of color
                for (int y = Globals.Image.Height - 1; y >= 0; y--)
                {
                    beforeFirstPixel = new Point { X = 0, Y = y - 1 };
                    for (int x = 0; x < Globals.Image.Width; x++)
                    {
                        // If color matches province, move on
                        if (Globals.Image.GetPixel(x, y) == province.Color)
                        {

                            firstPixel = new Point { X = x, Y = y };

                            // Easiest way to break out this loop. Meh
                            goto FoundFirstPixel;
                        }
                        beforeFirstPixel = new Point { X = x, Y = y };
                    }
                }

                FoundFirstPixel:
                outline.Add(firstPixel);
                boundaryPixel = firstPixel;
                backtrackPixel = beforeFirstPixel;
                currentPixel = Clockwise(backtrackPixel, boundaryPixel);

                while (!currentPixel.ToString().Equals(firstPixel.ToString()))
                {
                    if (currentPixel.Y >= 0 &&
                        currentPixel.X >= 0 &&
                        currentPixel.Y < Globals.Image.Height &&
                        currentPixel.X < Globals.Image.Width &&
                        Globals.Image.GetPixel(currentPixel.X, currentPixel.Y) == targetColor)
                    {
                        backtrackPixel = boundaryPixel;
                        boundaryPixel = currentPixel;
                        currentPixel = Clockwise(backtrackPixel, boundaryPixel);
                    }
                    else
                    {
                        outline.Add(currentPixel);
                        //if (currentPixel.X > -1 && currentPixel.Y > -1)
                        //{
                        //    Console.SetCursorPosition(currentPixel.X, currentPixel.Y);
                        //    Console.Write('X');
                        //}
                        backtrackPixel = currentPixel;
                        currentPixel = Clockwise(backtrackPixel, boundaryPixel);
                    }
                }

                // Change chunk color to white so it doesn't get searched again
                foreach (Point point in chunk)
                {
                    Globals.Image.SetPixel(point.X, point.Y, Color.White);
                }
            }

            // Recolors image
            foreach (Point point in chunks.SelectMany(x => x))
            {
                Globals.Image.SetPixel(point.X, point.Y, targetColor);
            }

            // Purge non-existent points
            outline = outline.Where(o => o.X > -1 &&
                                         o.Y > -1 &&
                                         o.X < Globals.Image.Width &&
                                         o.Y < Globals.Image.Height).ToList();

            // Get outline colors from image
            foreach (Point point in outline)
            {
                point.Color = Globals.Image.GetPixel(point.X, point.Y);
            }

            List<Color> uniqueColors = outline
                .GroupBy(x => x.Color)
                .Select(x => x.Key)
                .Where(x => x != targetColor)
                .Distinct().ToList();

            return Globals.Provinces.Where(x => uniqueColors.Contains(x.Color)).ToList();
        }

        private static void TestConsoleBuffer()
        {
            Console.BufferHeight = 1000;
            Console.BufferWidth = 1000;
        }

        /// <summary>
        /// Gets the point Clockwise to the current point
        /// [prev - target] + target;
        /// </summary>
        private static Point Clockwise(Point backtrack, Point boundary)
        {
            var offsetPoint = new Point
            {
                X = backtrack.X - boundary.X,
                Y = backtrack.Y - boundary.Y
            };

            // Gets the value from the dictionary using LINQ
            Point offset = ClockwiseOffset
                    .First(x => x.Key.X == offsetPoint.X &&
                                x.Key.Y == offsetPoint.Y).Value;

            return new Point()
            {
                X = offset.X + boundary.X,
                Y = offset.Y + boundary.Y
            };
        }
    }
}
