﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adjtest
{
    class Province
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Color Color { get; set; }
        public Dictionary<Tuple<int, int>, Point> Points = new Dictionary<Tuple<int, int>, Point>();
        public List<Province> Neighbors = new List<Province>();

        /// <summary>
        /// https://simpledevcode.wordpress.com/2015/12/29/flood-fill-algorithm-using-c-net/
        /// TODO: Is 4-directions a problem?
        /// TODO: http://will.thimbleby.net/scanline-flood-fill/ ???
        /// </summary>
        public List<List<Point>> GetProvinceChunks()
        {
            var chunks = new List<List<Point>>();
            var tempPoints = new List<Point>();
            int totalPixels = 0;

            tempPoints.AddRange(Points.Values.ToList());

            while (totalPixels < Points.Count)
            {
                var targetPixel = tempPoints.First();
                var chunk = GetChunk(targetPixel);
                chunks.Add(chunk);
                totalPixels += chunk.Count;

                tempPoints = tempPoints.Except(chunk).ToList();
            }

            // Resets color of chunks after fill
            foreach (var point in chunks.SelectMany(x => x))
            {
                Globals.Image.SetPixel(point.X, point.Y, Color);
            }

            return chunks;
        }

        public List<Point> GetChunk(Point point)
        {
            var targetColor = Color;
            var replacementColor = Color.White;
            var chunk = new List<Point>();

            var pixels = new Stack<Point>();

            pixels.Push(point);
            while (pixels.Count != 0)
            {
                Point temp = pixels.Pop();
                int y1 = temp.Y;
                while (y1 >= 0 && Globals.Image.GetPixel(temp.X, y1) == targetColor)
                {
                    y1--;
                }
                y1++;
                bool spanLeft = false;
                bool spanRight = false;
                while (y1 < Globals.Image.Height && Globals.Image.GetPixel(temp.X, y1) == targetColor)
                {
                    var chunkPoint = TryGetPoint(temp.X, y1);
                    chunk.Add(chunkPoint);
                    Globals.Image.SetPixel(temp.X, y1, replacementColor);

                    if (!spanLeft && temp.X > 0 && Globals.Image.GetPixel(temp.X - 1, y1) == targetColor)
                    {
                        pixels.Push(new Point { X = temp.X - 1, Y = y1 });
                        spanLeft = true;
                    }
                    else if (spanLeft && temp.X - 1 >= 0 && Globals.Image.GetPixel(temp.X - 1, y1) != targetColor)
                    {
                        spanLeft = false;
                    }
                    if (!spanRight && temp.X < Globals.Image.Width - 1 && Globals.Image.GetPixel(temp.X + 1, y1) == targetColor)
                    {
                        pixels.Push(new Point { X = temp.X + 1, Y = y1 });
                        spanRight = true;
                    }
                    else if (spanRight && temp.X < Globals.Image.Width - 1 && Globals.Image.GetPixel(temp.X + 1, y1) != targetColor)
                    {
                        spanRight = false;
                    }
                    y1++;
                }
            }
            return chunk;
        }

        /// <summary>
        /// Tries to get a point at the specified coordinates
        /// </summary>
        public Point TryGetPoint(int X, int Y)
        {
            Globals.Points.TryGetValue(Tuple.Create(X, Y), out Point point);
            return point;
        }
    }
}
